import { Article, ArticleService } from '../../client/services/article/articleservice';
import ApplicationComponent from '../../client/components/application/application';
import { Observable } from 'rxjs/Rx';

describe('Constructor', () => {
    it('should initialize articles', () => {
        let stub = {
            getArticles: ()  : Observable<Article[]> =>
            {
                return Observable.of([ new Article(), new Article(), new Article() ]);
            }
        };

        let target = new ApplicationComponent(stub);

        expect(target.articles.length).toBe(3, "Should have initialized length");
    });
});