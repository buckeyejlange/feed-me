var webpack = require('webpack');
var path = require('path');

module.exports = function(config) {
  config.set({
    autoWatch: true,
    browsers: ['Firefox'], //note: THIS IS CASE SENSITIVE. THAT IS SO DUMB. HOW MANY FIREFOX BROWSERS ARE THERE?!??!?!?!?!? AUUUUGGGHHHHH
    frameworks: ['jasmine'],
    files: [
      "vendor.ts", // needed for angular/other dependencies, MAKE SURE THIS IS FIRST FILE.
      { pattern: '*_spec.ts', watched: true },
      { pattern: '**/*_spec.ts', watched: true }
    ],

    preprocessors: {
      '*_spec.ts': ['webpack'],
      '**/*_spec.ts': ['webpack'],
      'vendor.ts': ['webpack']
    },

    plugins: [
      'karma-jasmine',
      'karma-firefox-launcher',
      'karma-webpack'
    ],

    // This is required or else Karma won't run any of the typescript specs.
    mime: {
      'text/x-typescript': ['ts','tsx']
    },
    singleRun: false,
    
    webpack: {
      module: {
          rules: [
              {
                  loader: 'ts-loader',
                  exclude: /node_modules/
              }
          ]
      },
      plugins: [
          new webpack.ContextReplacementPlugin(
              /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
              path.join(__dirname, './client-test') 
          )
      ],
      resolve: {
          extensions: [ ".ts", ".js" ]
      }
    },

    webpackMiddleware: {
    }
  });
};
