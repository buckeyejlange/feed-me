import { Injectable, ReflectiveInjector} from '@angular/core';
import {async, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, ConnectionBackend, Http, RequestOptions} from '@angular/http';
import {Response, ResponseOptions} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import { Article, ArticleService } from '../../../client/services/article/articleservice';

import { Observable } from 'rxjs/Rx';

describe('getArticles', () => {
    let injector: ReflectiveInjector;
    let target: ArticleService;
    let backend: MockBackend;
    let lastConnection: MockConnection;

    beforeEach(() => {
        this.injector = ReflectiveInjector.resolveAndCreate([
            { provide: ConnectionBackend, useClass: MockBackend },
            { provide: RequestOptions, useClass: BaseRequestOptions },
            Http,
            ArticleService
        ]);

        this.target = this.injector.get(ArticleService);
        this.backend = this.injector.get(ConnectionBackend) as MockBackend;
        this.backend.connections.subscribe((connection: any) => this.lastConnection = connection);
    });

    it('should retrieve articles', fakeAsync(() => {
        let article1 = new Article({ title: "Test1" });
        let article2 = new Article({ title: "Test2" });
        let results: Article[];

        this.target.getArticles().subscribe(arts => results = arts);
        this.lastConnection.mockRespond(new Response(new ResponseOptions({
            body: JSON.stringify([ article1, article2 ])
        })));
        tick(10);

        expect(results.length).toEqual(2);
        expect(results[0].title).toEqual(article1.title);
        expect(results[1].title).toEqual(article2.title);
    }));

    it('should call correct endpoint', () => {
        this.target.getArticles();
        expect(this.lastConnection).toBeDefined();
        expect(this.lastConnection.request.url).toMatch(/article/);
    });
});