import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

/**
 * Service for retrieving articles.
 * 
 * @class ArticleService
 * @constructor
 */
@Injectable()
export class ArticleService implements IArticleService {
    constructor(private http: Http) {

    }

    /**
     * Gets all available articles.
     */
    public getArticles(): Observable<Article[]> {
        return this.http.get("/article")
            .map(resp =>
            {
                return <Article[]> resp.json();
            });
    };
}

export interface IArticleService {
    getArticles(): Observable<Article[]>
}

/**
 * An individual article.
 * 
 * @class Article
 * @constructor
 */
export class Article {
    /**
     * A short description of the article contents.
     */
    public description: string;

    /**
     * The title of the article.
     */
    public title: string;

    /**
     * The URL where the article can be found.
     */
    public url: string;

    public constructor(init?:Partial<Article>) {
        Object.assign(this, init);
    }
}