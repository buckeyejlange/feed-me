import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import ApplicationComponent from './components/application/application';
import ArticleComponent from './components/article/article';
import HeaderComponent from './components/header/header';
import { ArticleService } from './services/article/articleservice';

/**
 * Angular module for the entire application.
 * 
 * @class AppModule
 */
@NgModule({
    imports:      [ BrowserModule, HttpModule ],
    declarations: [ 
            ApplicationComponent, 
            ArticleComponent,
            HeaderComponent
        ],
    bootstrap:    [ ApplicationComponent ]
})
export class AppModule { }