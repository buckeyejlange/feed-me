import { Component, Input } from '@angular/core';
import { Article } from '../../services/article/articleservice';

/**
 * An individual article rendered to the user.
 * 
 * @class ArticleComponent
 */
@Component({
    selector: 'article-item',
    templateUrl: './components/article/article.html',
    styleUrls: [ './components/article/article.css' ]
})
export default class ArticleComponent {
    /**
     * The article to render.
     */
    @Input() public article: Article;
}