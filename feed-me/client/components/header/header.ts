import { Component } from '@angular/core';

/**
 * The header for the applicaiton
 * 
 * @class HeaderComponent
 */
@Component({
    selector: 'app-header',
    templateUrl: './components/header/header.html',
    styleUrls: [ './components/header/header.css' ]
})
export default class HeaderComponent { }