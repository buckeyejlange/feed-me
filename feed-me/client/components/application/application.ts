import { Component, Inject } from '@angular/core';
import { Article, ArticleService, IArticleService } from '../../services/article/articleservice';

/**
 * The main application component.
 * 
 * @class ApplicationComponent
 * @constructor
 */
@Component({
    providers: [ ArticleService ],
    selector: 'feed-me',
    templateUrl: './components/application/application.html',
    styleUrls: [ './components/application/application.css' ]
})
export default class ApplicationComponent {
    /**
     * All articles to render in the application.
     */
    public articles: Article[];

    constructor(private articleService: IArticleService) {
        articleService.getArticles()
            .subscribe(articles => this.articles = articles);
    }
}