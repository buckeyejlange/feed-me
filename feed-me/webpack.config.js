var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: {
        app: './client/main.ts',
        vendor: './client/vendor.ts',
        
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, 'compiled/public')
    },
    module: {
        rules: [
            {
                loader: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "sass-loader" }
                ]
            }
        ]
    },
    plugins: [
        new webpack.ContextReplacementPlugin(
            /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
            path.join(__dirname, './client') 
        ),
        new CopyWebpackPlugin([
            { context: 'client', from: '**/*' }
        ],
        {
            ignore: [ '*.ts', 'tsconfig.json' ]
        })
    ],
    resolve: {
        extensions: [ ".tsx", ".ts", ".js" ]
    }
};