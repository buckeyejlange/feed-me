import { Article, ArticleAttribute } from '../data/article';

export class ArticleService {
    public GetArticlesAsync() : Promise<ArticleAttribute[]> {
        //return new Promise((fulfill, reject) => fulfill(this.GetArticles()));

        // keeping here as eventually this query will be more useful.
        return Article.findAll();
    }
}