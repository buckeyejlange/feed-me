import { IRouteConfiguration } from 'hapi';
import { ArticleService } from '../services/articleservice';

export default [
    <IRouteConfiguration> { 
        method: 'GET', 
        path: '/article/{id*}',
        handler: (request, reply) =>
        {
            try
            {
                let service = new ArticleService();
                reply(service.GetArticlesAsync());
            }
            catch (Exception)
            {
                reply(Exception);
            }
        }
    }
];