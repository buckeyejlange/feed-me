import * as Sequelize from 'sequelize';

// TODO: Come up with a cleaner solution to this.
let config = require('../config/config.json')[ process.env.NODE_ENV || 'development' ];
let sequelize = new Sequelize(config.database, config.username, config.password, { dialect: config.dialect, storage: config.storage });

/**
 * The configured sequelize instance.
 */
export default sequelize;