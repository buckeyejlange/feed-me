import * as Sequelize from 'sequelize';
import sequelize from './sequelize';

/**
 * Interface containing all attributes available to a Subscription entity.
 * 
 * @interface SubscriptionAttribute
 */
export interface SubscriptionAttribute {
    id?: number;
    title?: string;
    url?: string;
    createdAt: Date;
    updatedAt: Date;
}

/**
 * Interface containing all attributes of a Subscription instance.
 * 
 * @interface SubscriptionInstance
 */
export interface SubscriptionInstance extends Sequelize.Instance<SubscriptionAttribute>, SubscriptionAttribute {
}

/**
 * Interface containing all attributes of the Subscription model.
 * 
 * @interface SubscriptionModel
 */
export interface SubscriptionModel extends Sequelize.Model<SubscriptionInstance, SubscriptionAttribute> {
}

let Subscription = sequelize.define<SubscriptionInstance, SubscriptionModel>('subscription', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    title: Sequelize.STRING,
    url: Sequelize.STRING,
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE
});

/**
 * The Sequelize Subscription object.
 */
export { Subscription } 