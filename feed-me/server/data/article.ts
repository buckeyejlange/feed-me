import * as Sequelize from 'sequelize';
import sequelize from './sequelize';
import { Subscription } from './subscription';

/**
 * Interface containing all attributes available to an Article entity.
 * 
 * @interface ArticleAttribute
 */
export interface ArticleAttribute {
    id?: number;
    title?: string;
    description?: string;
    url?: string;
    subscription_id?: number;
    createdAt: Date;
    updatedAt: Date;
}

/**
 * Interface containing all attributes available to an Article Instance.
 * 
 * @interface ArticleInstance
 */
export interface ArticleInstance extends Sequelize.Instance<ArticleAttribute>, ArticleAttribute {
}

/**
 * Interface containing all attributes available to an Article model.
 * 
 * @interface ArticleModel
 */
export interface ArticleModel extends Sequelize.Model<ArticleInstance, ArticleAttribute> {
}

let Article = sequelize.define<ArticleInstance, ArticleModel>('article', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    title: Sequelize.STRING,
    description: Sequelize.STRING,
    url: Sequelize.STRING,
    subscription_id: {
        type: Sequelize.INTEGER,

        references: {
            model: Subscription,
            key: 'id'
        }
    },
    createdAt: Sequelize.DATE,
    updatedAt: Sequelize.DATE
});

/**
 * The Sequelize Article object.
 */
export { Article } 