'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

    return queryInterface.bulkInsert('subscriptions', [
      { Title: 'Test', Url: 'http://localhost/' }
    ])
    .then(sub => queryInterface.bulkInsert('articles', [
        { Title: 'Test Article 1', Description: 'I hope this works', Subscription_Id: sub },
        { Title: 'Test Article 2', Description: 'I REALLY hope this works', Subscription_Id: sub },
        { Title: 'Test Article 3', Description: 'Please work', Subscription_Id: sub }
      ]));
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
    return queryInterface.bulkDelete('articles', null, {})
      .then(queryInterface.bulkDelete('subscriptions', null, {}));
  }
};
