'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('subscriptions', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      title: { type: Sequelize.STRING },
      url: { type: Sequelize.STRING },
      createdAt: { type: Sequelize.DATE },
      updatedAt: { type: Sequelize.DATE }
    })
    .then(queryInterface.createTable('articles', { 
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      title: { type: Sequelize.STRING },
      description: { type: Sequelize.STRING },
      url: { type: Sequelize.STRING },
      subscription_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'subscriptions',
          key: 'Id'
        }
      },
      createdAt: { type: Sequelize.DATE },
      updatedAt: { type: Sequelize.DATE }
    }));
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('articles')
            .then(queryInterface.dropTable('subscriptions'));
  }
};
