#!/usr/bin/node

import * as Hapi from 'hapi';
import * as Inert from 'inert';
import * as Path from 'path';
import ArticleRoutes from './routes/articleroutes';

let server = new Hapi.Server({
    debug: {
        log: ['error'],
        request: ['error']
    }
});
server.connection({
    host: 'localhost',
    port: 8000
});

server.register(Inert, () => {
    server.route(ArticleRoutes);

    server.route([
        <Hapi.IRouteConfiguration> { 
            method: 'GET', 
            path: '/{param*}',
            handler: {
                directory: {
                    path: Path.join(__dirname, 'public'),
                    listing: true,
                    redirectToSlash: true
                } 
            }
        }
    ]);

    server.start((err) => {
        console.log('Server running at:', server.info.uri)
    });
});