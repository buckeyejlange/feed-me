# Feed-Me #

Feed-Me is a feed aggregator site.

### Technologies Used ###

* NodeJS v7.5.0 
* Angular 2
* TypeScript
* Webpack
* Jasmine
* Karma